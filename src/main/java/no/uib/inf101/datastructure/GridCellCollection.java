package no.uib.inf101.datastructure;

import java.util.List;

/**
 * Objects in a class extending GridCellCollection can assemble a list
 * of ojects through the getCells() method.
 * 
 * @param <T> the type of the element in the cell
 */
public interface GridCellCollection<T> {

    /**
     * Get a list containing the GridCell objects in this collection
     *
     * @return a list of all GridCell objects in this collection
     */
    List<GridCell<T>> getCells();

}
